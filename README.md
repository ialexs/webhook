#Bitbucket Webhook

###On the Web Server

Generate ssh keys for both the Apache user and the standard web server user (stephen in my case): `# ssh-keygen -t rsa`. Create these keys in the `.ssh` folder in the user's home folder. Ensure that the `.ssh` folder is assigned `0700` permissions and is owned by their respective user. The home folder for the Apache user is `/var/www` in most cases. Run `# cat /etc/passwd` or `# echo ~apache` to verify where the Apache user's home folder is actually located.

###On Bitbucket.org

- Add the deployment keys in the repository settings.
- Visit the "Services" section of the repository settings and add the "POST" url (ie. `http://somesite.com/webhook/hook.php?key=TsbWK7eZXGkxipXt6C`). This URL is the address you intend to use as the destination of the webhook on your server. The GET key is for added security.
- Ensure that the name of the repository is the same as the domain name (ie. `example.com`). This step ensures that the webhook will know which website is being updated.
- Copy the ssh url of the Git repository (ie. `git@Bitbucket.org:Username/example.com.git`).

###On the Web Server

In a public webroot on the server make a webhook folder with this structure:

	webhook
	|
	|-hook.php
	|
	|-webhook.log
	|
	|-.htaccess

Grant the Apache user ownership of this folder and its contents.

The `.htaccess` file is intended to block public access to the `webhook.log` file. Place the following code in the `.htaccess` file:

	<Files webhook.log>
	order deny,allow
	deny from all
	</Files>

Sample `hook.php` code:

	<?php

	class Site
	{
	    public static function checkout($site)
	    {
	        $git_folder = '/srv/www/' . $site . '/' . $site . '.git/';
	        if ($site == 'example.com')
	        {
	            shell_exec('cd ' . $git_folder . ' && GIT_WORK_TREE=/srv/www/example.com/public_html/wp-content/themes/html5-reset git checkout -f');
	        }
	        elseif ($site == 'example2.com')
	        {
	            shell_exec('cd ' . $git_folder . ' && GIT_WORK_TREE=/srv/www/example2.com/public_html git checkout -f');
	        }
	        elseif ($site == 'example3.com')
	        {
	            shell_exec('cd ' . $git_folder . ' && GIT_WORK_TREE=/srv/www/example3.com/public_html/wp-content/themes/html5-reset git checkout -f');
	        }
	    }
	}

	if ($_REQUEST['key'] == 'TsbWK7eZXGkxipXt6C' && isset($_REQUEST['payload']))
	{
	    $payload = json_decode($_REQUEST['payload']);
	    $branch = $payload->commits[0]->branch;
	    $site_name = $payload->repository->slug;
	    if (!isset($branch) || !isset($site_name))
	    {
	        exit();
	    }
	    elseif ($branch == 'master')
	    {
	        $git_dir = '/srv/www/' . $site_name . '/' . $site_name . '.git/';
	        $output = shell_exec('cd ' . $git_dir . ' && git reset --hard HEAD && git pull origin master:master');
	        $status = shell_exec('cd ' . $git_dir . ' && git rev-parse --short HEAD');

	        //Log the request and result
	        file_put_contents('webhook.log', print_r($payload, TRUE) . "\n$output\nCurrent hash is $status\n################################################\n", FILE_APPEND);

	        //Put code into production
	        Site::checkout($site_name);
	    }
	}

Change directories to the folder containing your website on your server (`# cd /srv/www/example.com`). Do not navigate into the public webroot.

Create a folder containing the domain name of the site with ".git" appended to the name (`# mkdir example.com.git`). Change directories into this newly created folder and issue commands:

- `# git init` to initialize the repo.
- Add the remote Bitbucket repository: `# git remote add origin git@Bitbucket.org:Username/example.com.git`.
- Verify that the remote was successfully added: `# git remote -v`. Ensure that ports 22 and 9418 are open for Git to contact remotes.
- Issue an initial pull request: `# git pull origin master:master`. If everything is configured properly the pull will complete successfully.

Make Apache the owner of the `example.com.git` folder: `# chown apache:apache -R /srv/www/example.com/example.com.git`. Finally, make Apache the owner of the public web folder that the Git repository will checkout into: `# chown apache:apache -R /srv/www/example.com/public_html`.